This was created based on this article/post: https://trello.com/c/h1J3RCL7/13-python-programming

For Python3! (not for Python2.7)

2018. October, peter.vago@gmail.com


----------------------------------------
linked post is very good, but '*' characters have benn missing from the code. It was a bit tricky to make it work.

Hi,
I modified the bitlibmodule.c to run with Python3. It works well !!
Just do this :
change PyString_FromFormat to PyUnicode_FromFormat,
PyInt_FromLong to Py_Long_FromLong

and modify the Init routine like this :

PyMODINIT_FUNC PyInit_bitlib ( void ) { / initialise module /
PyObject module; int i, n;
static struct { char N; int V; } T[] = {
{"BL_ASK", BL_ASK},
{"BL_ASYNCHRONOUS", BL_ASYNCHRONOUS},
{"BL_COUNT_ANALOG", BL_COUNT_ANALOG},
{"BL_COUNT_DEVICE", BL_COUNT_DEVICE},
{"BL_COUNT_LOGIC", BL_COUNT_LOGIC},
{"BL_COUNT_RANGE", BL_COUNT_RANGE},
{"BL_MAX_RATE", BL_MAX_RATE},
{"BL_MAX_SIZE", BL_MAX_SIZE},
{"BL_MAX_TIME", BL_MAX_TIME},
{"BL_MODE_FAST", BL_MODE_FAST},
{"BL_MODE_DUAL", BL_MODE_DUAL},
{"BL_MODE_LOGIC", BL_MODE_LOGIC},
{"BL_MODE_MIXED", BL_MODE_MIXED},
{"BL_MODE_STREAM", BL_MODE_STREAM},
{"BL_SELECT_CHANNEL", BL_SELECT_CHANNEL},
{"BL_SELECT_DEVICE", BL_SELECT_DEVICE},
{"BL_SELECT_SOURCE", BL_SELECT_SOURCE},
{"BL_SOURCE_ALT", BL_SOURCE_ALT},
{"BL_SOURCE_BNC", BL_SOURCE_BNC},
{"BL_SOURCE_GND", BL_SOURCE_GND},
{"BL_SOURCE_POD", BL_SOURCE_POD},
{"BL_SOURCE_X10", BL_SOURCE_X10},
{"BL_SOURCE_X20", BL_SOURCE_X20},
{"BL_SOURCE_X50", BL_SOURCE_X50},
{"BL_COUPLING_DC", BL_COUPLING_DC},
{"BL_COUPLING_AC", BL_COUPLING_AC},
{"BL_COUPLING_RF", BL_COUPLING_RF},
{"BL_STATE_ACTIVE", BL_STATE_ACTIVE},
{"BL_STATE_DONE", BL_STATE_DONE},
{"BL_STATE_ERROR", BL_STATE_ERROR},
{"BL_STATE_IDLE", BL_STATE_IDLE},
{"BL_SYNCHRONOUS", BL_SYNCHRONOUS},
{"BL_TRACE_FORCED", BL_TRACE_FORCED},
{"BL_TRACE_FOREVER", BL_TRACE_FOREVER},
{"BL_TRIG_FALL", BL_TRIG_FALL},
{"BL_TRIG_HIGH", BL_TRIG_HIGH},
{"BL_TRIG_LOW", BL_TRIG_LOW},
{"BL_TRIG_NONE", BL_TRIG_NONE},
{"BL_TRIG_RISE", BL_TRIG_RISE},
{"BL_VERSION_BINDING", BL_VERSION_BINDING},
{"BL_VERSION_DEVICE", BL_VERSION_DEVICE},
{"BL_VERSION_LIBRARY", BL_VERSION_LIBRARY},
{"BL_ZERO", BL_ZERO}
};
static struct PyModuleDef moduledef = {
PyModuleDef_HEAD_INIT,
"bitlib",
bitlib_doc,
-1,
bitlib_methods,
NULL,
NULL,
NULL,
NULL,
};

module = PyModule_Create(&moduledef);

n = sizeof(T) / sizeof(T[0]);
for ( i = 0; i < n; i++ )
    PyModule_AddIntConstant(module,T[i].N, T[i].V);

return module;

}

